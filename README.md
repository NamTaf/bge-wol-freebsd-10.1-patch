# README #

### What is this repository for? ###

* Patch for FreeBSD 10.1 to enable Wake on Lan on bge interfaces 
* Adapted from the nas4free patch for 9.1
* Additional modifications to make patch suit svn release 282752

### How do I get set up? ###

* cd  /usr/src/sys/dev/bge/
* patch if_bge.c < if_bge.c.patch

* cd /usr/src/sys/modules/bge/
* make
* make install

* cd /boot/kernel/
* rm if_bge.ko
* cp /usr/src/sys/modules/bge/if_bge.ko ./

* /etc/rc.d/netif restart (will require reconnecting via SSH)
* ifconfig (look for WOL_MAGIC option)

### I am having troubles compiling! ###

* cd /
* rm -rf /usr/src/
* svn co https://svn0.us-east.freebsd.org/base/releng/10.1/ /usr/src/

Wait for the source tree to clone, then repeat all of the previous steps to patch again.

### Thanks to zoon01 and daoyama for NAS4Free 9.1 patch, w4w for 10.1 patch ###

* free4nas 9.1. Patch on Sourceforge [http://sourceforge.net/p/nas4free/code/HEAD/tree/branches/9.1.0.1/build/kernel-patches/bge/files/patch-if_bge.diff#l25](http://sourceforge.net/p/nas4free/code/HEAD/tree/branches/9.1.0.1/build/kernel-patches/bge/files/patch-if_bge.diff#l25)